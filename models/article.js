const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    slug: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    body: { type: String, required: true },
    tagList: [String],
    // favorited: { type: Boolean, required: true },
    favoritesCount: { type: Number, required: true },
    author: { type: Schema.Types.ObjectId, ref: "User" },
    comments: [
      {
        commentId: { type: Schema.Types.ObjectId, ref: "Comment" },
      },
    ],
    usersFav: [{ userId: { type: Schema.Types.ObjectId, ref: "User" } }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("Article", articleSchema);
