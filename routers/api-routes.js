const express = require("express");
const { body } = require("express-validator");
const User = require("../models/user");

const authController = require("../controllers/auth-controller");
const articlesController = require("../controllers/articles-controller");
const commentController = require("../controllers/comments-controller");
const isAuth = require("../middlewares/is-auth");

const router = express.Router();

// auth

router.post(
  "/users/login",
  [
    body("email")
      .isEmail()
      .withMessage("please enter a valid email")
      .normalizeEmail()
      .custom((value, { req }) => {
        return User.findOne({ email: value }).then((user) => {
          if (!user) {
            return Promise.reject("Email does not exist");
          }
        });
      }),
    body("password", "please enter a valid password")
      .isLength({ min: 3 })
      .isAlphanumeric()
      .trim(),
  ],
  authController.postLogin
);

router.get("/user", authController.getUser);

router.get("/profiles/:username", authController.getProfile);

router.post(
  "/users",
  [
    body("email")
      .isEmail()
      .withMessage("please enter a valid email")
      .normalizeEmail()
      .custom((value, { req }) => {
        return User.findOne({ email: value }).then((user) => {
          if (user) {
            return Promise.reject("Email exists aleardy");
          }
        });
      }),
    body("username")
      .not()
      .isEmpty()
      .withMessage("please enter a valid username")
      .trim(),
    body("password", "please enter a valid password")
      .isLength({ min: 3 })
      .isAlphanumeric(),
  ],
  authController.postUser
);

router.put(
  "/user",
  isAuth,
  [
    body("email")
      .isEmail()
      .withMessage("please enter a valid email")
      .normalizeEmail()
      .custom((value, { req }) => {
        return User.findOne({ email: value }).then((user) => {
          if (user) {
            return Promise.reject("Email exists aleardy");
          }
        });
      }),
    body("username")
      .isEmpty()
      .withMessage("please enter a valid username")
      .trim(),
    body("bio").isEmpty().withMessage("please enter a valid bio"),
  ],
  authController.putUpdateUser
);

router.post(
  "/profiles/:username/follow",
  isAuth,
  authController.postFullowUser
);

router.delete(
  "/profiles/:username/follow",
  isAuth,
  authController.deleteUnFullowUser
);

// articles

router.put(
  "articles/:slug",
  isAuth,
  [
    body("title").not().isEmpty(),
    body("description").not().isEmpty(),
    body("body").not().isEmpty(),
  ],
  articlesController.putUpdateArticle
);

router.delete("articles/:slug", isAuth, articlesController.deletArticle);

router.get("/articles/:slug", articlesController.getArticle);

router.get("/articles", articlesController.getArticles);

router.post(
  "/articles",
  isAuth,
  [
    body("title").not().isEmpty(),
    body("description").not().isEmpty(),
    body("body").not().isEmpty(),
  ],
  articlesController.postArticle
);

router.get("/articles/feed", isAuth, articlesController.getFeedArticles);

// favorite

router.post(
  "/articles/:slug/favorite",
  isAuth,
  articlesController.postFavoriteArticles
);

router.delete(
  "/articles/:slug/favorite",
  isAuth,
  articlesController.deleteUnFavoriteArticles
);

// tages

router.get("/tags", articlesController.getTags);

// comments

router.post(
  "/articles/:slug/comments",
  isAuth,
  body("comment").not().isEmpty(),
  commentController.postAddCommentToArticle
);

router.delete(
  "/articles/:slug/comments/:id",
  isAuth,
  commentController.deleteCommentFromArticle
);

router.get(
  "/articles/:slug/comments",
  commentController.getCommentsFromArticle
);

module.exports = router;
