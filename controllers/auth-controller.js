const User = require("../models/user");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

exports.getUser = (req, res, next) => {};

exports.postLogin = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array(),
      },
    });
  }
  let loadedUser;
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        const error = new Error("User could not be found");
        error.httpStatusCode = 401;
        throw error;
      }
      loadedUser = user;
      return bcrypt.compare(req.body.password, user.password);
    })
    .then((doMatch) => {
      if (doMatch) {
        if (!doMatch) {
          const error = new Error("Wrong password");
          error.httpStatusCode = 401;
          throw error;
        }
        const token = jwt.sign(
          {
            email: loadedUser.email,
            userId: loadedUser._id.toString(),
          },
          "youneslounis",
          { expiresIn: "1h" }
        );
        console.log(token);

        res.status(200).json({
          user: {
            email: loadedUser.email,
            token: token,
            username: loadedUser.username,
            bio: loadedUser.bio,
            image: loadedUser.image,
          },
        });
      }
    })
    .catch((err) => {
      const error = new Error(err.msg);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postUser = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array(),
      },
    });
  }
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  bcrypt
    .hash(req.body.password, 12)
    .then((hachedPassword) => {
      const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: hachedPassword,
      });
      return user.save();
    })
    .then((user) => {
      res.status(201).json({ user: user });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.putUpdateUser = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array,
      },
    });
  }
  if (!req.file) {
    return res.status(422).json({
      errors: {
        body: "File not uploded",
      },
    });
  }

  const username = req.body.username;
  const email = req.body.email;
  const bio = req.body.bio;
  const image = req.file.path;
  User.findOne({ email: email })
    .then((user) => {
      clearImage(user.image);
      user.username = username;
      user.email = email;
      user.image = image;
      user.bio = bio;
      return user.save();
    })
    .then((result) => {})
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getProfile = (req, res, next) => {
  const username = req.parms.username;
  User.find({ username: username })
    .then((user) => {
      res.status(200).json({
        profile: {
          username: user.username,
          bio: user.bio,
          image: user.image,
          following: req.user.IFollow,
        },
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postFullowUser = (req, res, next) => {
  const username = req.parms.username;
  User.findOne({ username: username })
    .then((user) => {
      req.user.IFollow.push(user);
      return req.user.save();
    })
    .then((result) => {
      res.status(201).json({});
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.deleteUnFullowUser = (req, res, next) => {
  const username = req.params.username;
  User.findOne({ username: username })
    .then((user) => {
      req.user.IFollow.pull(user._id);
      return req.user.save();
    })
    .then((result) => {
      res.status(201).json({});
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};
