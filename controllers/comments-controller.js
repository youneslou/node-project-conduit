const Article = require("../models/article");
const Comment = require("../models/comment");
const { validationResult } = require("express-validator");

exports.postAddCommentToArticle = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array,
      },
    });
  }
  let updatedArticle;
  const slug = req.params.slug;
  Article.find({ slug: slug })
    .then((article) => {
      updatedArticle = article;
      const comment = new Comment({
        body: req.body.body,
        author: req.user,
      });
      return comment.save();
    })
    .then((resultComment) => {
      updatedArticle.comments.push(resultComment);
      return updatedArticle.save();
    })
    .then((result) => {})
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.deleteCommentFromArticle = (req, res, next) => {
  const slug = req.params.slug;
  const id = req.params.id;
  Article.findOne({ slug: slug })
    .then((article) => {
      article.comments.pull(id);
      return article.save();
    })
    .then((result) => {
      res.status(201).json({});
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getCommentsFromArticle = (req, res, next) => {
  const slug = req.params.slug;
  Article.find({ slug: slug })
    .populate("comments")
    .then((articles) => {
      res.status(200).json({
        comments: articles.Comments,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};
