const Article = require("../models/article");
const User = require("../models/user");
const { validationResult } = require("express-validator");

exports.getArticles = (req, res, next) => {
  const limit = req.query.limit || 10;
  const offset = req.query.offset || 0;
  const author = req.query.author;
  const tag = req.query.tag;
  const favorited = req.query.favorited;

  let totalCount;
  if (author) {
    User.findOne({ username: author })
      .then((user) => {
        return Article.find({ author: user._id })
          .populate("author")
          .skip(offset)
          .limit(limit);
      })
      .then((articles) => {
        res.status(200).json({
          articles: articles,
          articlesCount: articles.length,
        });
      })
      .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
  } else if (tag) {
    Article.find({ tagList: { $in: [tag] } })
      .skip(offset)
      .limit(limit)
      .then((articles) => {
        res.status(200).json({
          articles: articles,
          articlesCount: articles.length,
        });
      })
      .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
  } else if (favorited) {
    Article.find({ usersFav: { $in: [favorited] } })
      .skip(offset)
      .limit(limit)
      .then((articles) => {
        res.status(200).json({
          articles: articles,
          articlesCount: articles.length,
        });
      })
      .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
  } else {
    Article.countDocuments()
      .then((count) => {
        totalCount = count;
        return Article.find().populate("author").skip(offset).limit(limit);
      })
      .then((articles) => {
        res.status(200).json({
          articles: articles,
          articlesCount: totalCount,
        });
      })
      .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
  }
};

exports.getArticle = (req, res, next) => {
  console.log(typeof req.params.slug);
  Article.findOne({ slug: req.params.slug })
    .populate("author")
    .then((article) => {
      res.status(200).json({
        article: article,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postArticle = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array(),
      },
    });
  }
  const article = new Article({
    slug: req.body.title.split(" ").join("-"),
    title: req.body.title,
    description: req.body.description,
    body: req.body.body,
    tagList: req.body.tagList,
    author: req.user,
    favoritesCount: 0,
  });
  article
    .save()
    .then((article) => {
      res.status(201).json({
        article: article,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.putUpdateArticle = (req, res, next) => {
  console.log(req.params.slug);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: {
        body: errors.array(),
      },
    });
  }

  Article.find({ slug: req.params.slug })
    .then((article) => {
      console.log(article);
      article.slug = req.bady.title.split(" ").join("-");
      article.title = req.bady.title;
      article.description = req.body.description;
      article.body = req.body.bady;
      article.tagList = req.body.tagList;
      return article.save();
    })
    .then((article) => {
      res.status(201).json({
        article: article,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.deletArticle = (req, res, next) => {
  console.log(req.url);
  const slug = req.params.slug;
  Article.findOneAndDelete({ slug: slug })
    .then((result) => {
      res.status(201).json({});
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getFeedArticles = (req, res, next) => {
  Article.find({ author: { $all: req.user.IFollow } })
    .then((articles) => {
      res.status(200).json({
        articles: articles,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postFavoriteArticles = (req, res, next) => {
  const slug = req.params.slug;
  Article.findOne({ slug: slug })
    .then((article) => {
      article.usersFav.push(req.user);
      article.favoritesCount = favoritesCount + 1;
      return article.save();
    })
    .then((artilce) => {
      res.status(201).json({
        article: article,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.deleteUnFavoriteArticles = (req, res, next) => {
  const slug = req.params.slug;
  Article.findOne({ slug: slug })
    .then((article) => {
      article.usersFav.pull(req.user._id);
      article.favoritesCount = favoritesCount - 1;
      return article.save();
    })
    .then((result) => {
      res.status(201).json({});
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getTags = (req, res, next) => {
  Article.find()
    .select("tagList")
    .then((tags) => {
      res.status(200).json({ tags: tags });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};
