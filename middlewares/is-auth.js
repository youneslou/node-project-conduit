const jwt = require("jsonwebtoken");
const User = require("../models/user");

module.exports = (req, res, next) => {
  const authHeader = req.get("Authorization");
  if (!authHeader) {
    const error = new Error("Not Authenticated.");
    error.httpStatusCode = 401;
    throw error;
  }
  const token = authHeader.split(" ")[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, "youneslounis");
  } catch (error) {
    error.httpStatusCode = 500;
    throw error;
  }
  if (!decodedToken) {
    const error = new Error("Not Authenticated");
    error.httpStatusCode = 401;
    throw error;
  }
  User.findOne({ _id: decodedToken.userId })
    .then((user) => {
      console.log("auth...............................");
      req.user = user;
      next();
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};
