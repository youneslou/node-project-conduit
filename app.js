const express = require("express");
const mongoose = require("mongoose");
const User = require("./models/user");
const multer = require("multer");
const Path = require("path");
// const cors = require("cors");

const apiRoutes = require("./routers/api-routes");

const app = express();
// app.use(cors());

// multer files
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString + "-" + file.originalname);
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(express.json());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH"
  );
  res.setHeader("Access-Control-Allow-Headers", "*");
  next();
});
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("image")
);
app.use("/images", express.static(Path.join(__dirname, "images")));

app.use((error, req, res, next) => {
  console.log("from error middlewares", error);
  res.status(500).json({
    errors: {
      body: error.msg,
    },
  });
});

app.use((req, res, next) => {
  console.log(req.method, req.url);
  next();
});
app.use("/api", apiRoutes);

mongoose
  .connect("mongodb://127.0.0.1:27017/conduit")
  .then((result) => {
    app.listen(8080);
  })
  .catch((err) => {
    console.log(err);
  });
